print('hello')
import os
import random
import sys

import constructor
import districtingGraph
import initializer
import mergeSplit
import metropolisHastings
import numpy as np
# import dataWriter

from importlib import reload
print('hello')
reload(constructor)
reload(districtingGraph)
reload(initializer)
reload(mergeSplit)
reload(metropolisHastings)

print('initializing run...')
state, args = initializer.setRunParametersFromCommandLine(sys.argv)

proposal, args = mergeSplit.define(args)
info = args

info = initializer.fillMissingInfoFields(info)
state = initializer.determineStateInfo(state, info)
state = readPlan(state, info, file)
####################
def readPlan(state, info, file)
    zoneTobg = g(file)
    for zone in zoneTobg:
        state.districts[zone] = state['graph'].subgraph(zoneTobf[zone])
    constructor.assignDistricts(state.districts, state)
    # checks
    popCheck = constraints.checkPopulation(state)
    distCheck = len(districts) == numDist
####################


# state = constructor.contructPlan(state, info)

import energy
reload(energy)
computeEnergy = energy.getEnergyFunction(info)
print(computeEnergy(state, info))
exit()

print('run initialized...')

print('starting chain...')
state = metropolisHastings.run(state, proposal, info)
print('finishing chain...')
