import networkx as nx
import operator

import constraints
import energy
import initializer
import tree

import importlib

importlib.reload(constraints)
importlib.reload(energy)
importlib.reload(initializer)
importlib.reload(tree)


def searchForDistrict(graph, rng, state, info, remainingPop, attemptPerDist, 
                      boolop):    
    for attempt in range(attemptPerDist):
        graphTree = tree.wilson(graph, rng)

        cutSet, edgeWeights = {}, {}
        try:
            cutSet, edgeWeights = tree.edgeCuts(graphTree, remainingPop, state, 
                                                info, boolop)
        except:
            state = initializer.determineStateInfo(state, info)
            cutSet, edgeWeights = tree.edgeCuts(graphTree, remainingPop, graph, 
                                                info, boolop)
        if cutSet:
            break
    return cutSet, edgeWeights, graphTree

def extractDistrictsFromCutTree(graphTree, graph, state, districts):
    distGraph = state["graph"]
    newGraph = graph.copy()

    for conComp in nx.connected_components(graphTree):
        popConComp = sum([distGraph.nodes[n]["Population"] 
                          for n in conComp])
        if state["minPop"] <= popConComp <= state["maxPop"]:
            districts[len(districts)] = graph.subgraph(conComp).copy()
            newGraph.remove_nodes_from(conComp)
    return districts, newGraph

def assignDistricts(districts, state):
    state["districts"] = districts

    nodeToDistrict = {}
    for dist in districts:
        for n in districts[dist].nodes:
            nodeToDistrict[n] = dist
    state["nodeToDistrict"] = nodeToDistrict

    return state

def checkOverlap(districts, info):
    nonIntersectingNodes = info["constraints"]["nonIntersectingNodes"]
    for distInd in districts:
        district = districts[distInd]
        nodesInDist = nonIntersectingNodes.intersection(set(district.nodes))
        if len(nodesInDist) > 1:
            return False
    return True

def attemptMakePlan(state, info, attemptPerDist = 100):
    if "seedAttemptsByDist" in info["parameters"]:
        attemptPerDist = info["parameters"]["seedAttemptsByDist"]
    numDist = info["parameters"]["districts"]
    rng = info["rng"]
    distGraph = state["graph"]
    graph = distGraph.copy()

    districts = {}
    
    cuts = numDist - 1
    for distCut in range(cuts): 
        nextDistrictNotFound = True
        counter = 0
        while nextDistrictNotFound and counter < attemptPerDist:
            boolop = operator.and_ if distCut == cuts-1 else operator.or_

            # print()
            remainingPop = sum([distGraph.nodes[n]["Population"] 
                                for n in graph.nodes])
            
            cutSet, edgeWeights, graphTree = searchForDistrict(graph, rng, state, 
                                                               info, remainingPop,
                                                               attemptPerDist, 
                                                               boolop)
            if not cutSet:
                return state, False
        
            orderedCutSet = sorted(list(cutSet), 
                                   key=lambda e:''.join(sorted(list(e))))
            e = rng.choice(orderedCutSet)
            
            graphTree.remove_edge(*e)

            districts, newGraph = extractDistrictsFromCutTree(graphTree, graph, state, 
                                                              districts)
            if checkOverlap(districts, info):
                nextDistrictNotFound = False
                graph = newGraph
            else:
                del districts[len(districts)-1]
                counter += 1

    state = assignDistricts(districts, state)
    
    popCheck = constraints.checkPopulation(state)
    distCheck = len(districts) == numDist
    
    return state, popCheck and distCheck

def contructPlan(state, info, maxAttempts = 1000):
    '''Finds a random districting.'''

    for attempt in range(maxAttempts):
        state, success = attemptMakePlan(state, info)
        print("attempt", attempt)
        if success:
            break
    if not success:
        raise Exception("Could not find acceptable initial state")
    return state