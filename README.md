# school-segregation-durham

This repository contains data and code for the Duke MIDS capstone project - School segregation in Durham

**mergesplitcodebase-master**: Redistricting simulation
**redistricting-meta**: File generators for redistricting
**time-series-study**: EDA
**Census-Data-Study**: EDA

** The paper can be viewed [here](https://docs.google.com/document/d/1W6UahPOs0dXXQRLTF8DwWrOGnYGc9WP3rhz-uwNO_1U/edit) **
