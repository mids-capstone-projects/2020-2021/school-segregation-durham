import os
from pathlib import Path
import pandas as pd
import glob
import geopandas as gpd

SHAPE_FILE_LOCATION               =              "/shapefiles/Public_Elementary_School_Base_Assignment_Zones.shp"
DATA_FILE_LOCATIONS               =              "/scraper/dpsnc/cleaned_csvs/page2/"
GIF_DIRECTORY                     =              "/gifs/"
DATA_DIRECTORY                    =              "/nc-school-report-card/src_datasets2020-08/"  

class DataLoader():
    def __init__(self):
        self.dfs = {}
    
    def get_directory_path(self):
        return os.path.dirname(os.path.realpath("__file__"))

    def gif_directory(self):
        return self.get_directory_path()+GIF_DIRECTORY

    def parent_directory(self, path):
        return str(Path(path).parent)
    
    def filenames(self, path):
        return glob.glob(path+"*.csv")
    
    def load_enrollment_files(self):
        directory_path = self.get_directory_path()
        parent_directory = self.parent_directory(directory_path)
        data_directory = parent_directory+DATA_FILE_LOCATIONS
        return self.filenames(data_directory)
    
    def generate_dict_key(self, file):
        return file.split("/")[-1][:-4]
    
    def process_file(self, file):
        df = pd.read_csv(file)
        key = self.generate_dict_key(file)
        self.dfs[key] = df

    def load_enrollment_data(self):
        enrollment_files = self.load_enrollment_files()
        for file in enrollment_files:
            self.process_file(file)
        return self.dfs

    def load_elementary_school_district_shapefile(self):
        directory_path = self.get_directory_path()
        shape_file_location = directory_path+SHAPE_FILE_LOCATION
        return gpd.read_file(shape_file_location)
    
    def load_nc_report_card_df(self, filename):
        directory_path = self.get_directory_path()
        data_file_location = directory_path+DATA_DIRECTORY+filename
        return pd.ExcelFile(data_file_location).parse('Sheet1')
        







    
    

