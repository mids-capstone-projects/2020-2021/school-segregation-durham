import pandas as pd
import geopandas as gpd

DATA_MAP =  {
                "2010": 'enrollment_10_11',
                "2011": 'enrollment_11_12',
                "2012": 'enrollment_12_13',
                "2013": 'enrollment_13_14',
                "2014": 'enrollment_14_15',
                "2015": 'enrollment_15_16',
                "2016": 'enrollment_16_17',
                "2017": 'enrollment_17_18',
                "2018": 'enrollment_18_19',
                "2019": 'enrollment_19_20'
            }
VERSION_3_FILES = ["2016","2017","2018","2019"]
VERSION_2_FILES = ["2015"]
VERSION_1_FILES = ["2010","2011","2012","2013","2014"]

REPLACEMENT_MAP = {
                    "Whte M"        :           "White M",
                    "Whte F"        :           "White F",
                    "Blk M"         :           "Black M",
                    "Blk F"         :           "Black F",
                    "Hisp M"        :           "Hispanic M",
                    "Hisp F"        :           "Hispanic F",
                    "Asian M"       :           "Asian M",
                    "Asian F"       :           "Asian F",
                    "Am Ind M"      :           "American Indian M",
                    "Am Ind F"      :           "American Indian F",
                    "Multi M"       :           "Multi Racial M",
                    "Multi F"       :           "Multi Racial F",
                    "Haw Pac M"     :           "Hawaiian Pacific M",
                    "Haw Pac F"     :           "Hawaiian Pacific F",
                    "Sum M"         :           "Totals Male",
                    "Sum F"         :           "Totals Female",
                    "Sum TOTAL"     :           "Totals All"
                }

TOTAL_COLS   = {
                    "White"             :           ["White M", "White F"],
                    "Asian"             :           ["Asian M", "Asian F"],
                    "Black"             :           ["Black M", "Black F"],
                    "Hispanic"          :           ["Hispanic M", "Hispanic F"],
                    "Multi Racial"      :           ["Multi Racial M", "Multi Racial F"],
                    "Hawaiian Pacific"  :           ["Hawaiian Pacific M", "Hawaiian Pacific F"],
                    "American Indian"   :           ["American Indian M", "American Indian F"],
                    "Multi Racial"      :           ["Multi Racial M", "Multi Racial F"]
                }

class DataProcessor():

    def __init__(self, dfs, shapefile):
        self.dfs = dfs
        self.shapefile = shapefile.astype({"facilityid": int, "stateid": int})
        self.data = {}


    def process_version_3_and_2_file(self, file_code, filename, version):
        required_df = self.dfs[filename]
        required_df = required_df.astype({"Site Code": int})
        if version == 3:
            required_df = required_df.drop(columns=['School'])
        new_shapefile = self.shapefile.merge(required_df, left_on = "stateid", right_on = "Site Code", how = "left")
        self.data[file_code] = new_shapefile

    def process_version_3_files(self):
        for file in VERSION_3_FILES:
            filename = DATA_MAP[file]
            self.process_version_3_and_2_file(file, filename, 3)


    def process_version_2_files(self):
        for file in VERSION_2_FILES:
            filename = DATA_MAP[file]
            return self.process_version_3_and_2_file(file, filename, 2)

    def add_total_column(self, required_df):
        for col in TOTAL_COLS:
            values = TOTAL_COLS[col]
            if values[0] in required_df.columns:
                required_df[col+" Total"] = required_df[values[0]] + required_df[values[1]]
                required_df[col+" %"] = (required_df[col+" Total"]/required_df["Totals All"])*100
        return required_df

    def process_version_1_file(self, file_code, filename):
        required_df = self.dfs[filename]
        required_df = required_df.astype({"Site Code": str})
        # required_df = required_df.drop(columns=['Schools'])
        required_df = required_df.rename(columns = REPLACEMENT_MAP)
        required_df['Site Code'] = '320' + required_df['Site Code'].astype(str)
        required_df = required_df.astype({"Site Code": float})
        required_df = required_df.astype({"Site Code": int})
        required_df = self.add_total_column(required_df)
        new_shapefile = self.shapefile.merge(required_df, left_on = "stateid", right_on = "Site Code", how = "left")
        self.data[file_code] = new_shapefile

    def process_version_1_files(self):
        for file in VERSION_1_FILES:
            filename = DATA_MAP[file]
            self.process_version_1_file(file, filename)

    
    def process(self):
        self.process_version_1_files()
        self.process_version_2_files()
        self.process_version_3_files()
        return self.data


    
    

